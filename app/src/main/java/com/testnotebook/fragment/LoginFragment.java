package com.testnotebook.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import com.testnotebook.databinding.FragmentLoginBinding;
import com.testnotebook.helper.Lo;
import com.testnotebook.helper.Utils;
import com.testnotebook.interfaces.OnAuthFragInteractionListener;


public class LoginFragment extends BaseFragment implements View.OnClickListener {
    public final com.testnotebook.helper.Lo Lo = new Lo(this);


    private OnAuthFragInteractionListener authActivityListener;
    private FragmentLoginBinding binding;


    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance(String email) {
        LoginFragment fragment = new LoginFragment();
        if (email != null) {
            Bundle bn = new Bundle();
            bn.putString("email", email);
            fragment.setArguments(bn);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);

        if (getArguments() != null && getArguments().getString("email") != null)
            binding.email.setText(getArguments().getString("email"));

        binding.btnSigninEmail.setOnClickListener(this);
        binding.btnSignup.setOnClickListener(this);
        binding.btnForgotPswd.setOnClickListener(this);
        return binding.getRoot();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAuthFragInteractionListener) {
            authActivityListener = (OnAuthFragInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        authActivityListener = null;
    }

    @Override
    public void onClick(View v) {
        if (!Utils.isConnectingToInternet(true, R.string.error_label_no_internet))
            return;

        switch (v.getId()) {
            case R.id.btnSigninEmail:
                logInEmail();
                break;
            case R.id.btnForgotPswd:
                ResetEmailDialog.newInstance(binding.email.getText().toString()).show(getActivity().getSupportFragmentManager(), "ResetEmailDialog");
                break;
            case R.id.btnSignup:
                if (getActivity() != null)
                    authActivityListener.onShowSignUpFrag();
                break;

        }

    }


    private void logInEmail() {

        //  Lo.g("# logInEmail "+ Utils.isEmailValid(binding.email));
        //  Lo.g("# logInEmail "+ isPasswordValid());
        if (!Utils.isEmailValid(binding.email) || !isPasswordValid())
            return;

        // Lo.g("# logInEmail ");
        showProgressBar(true, R.string.progress_login);

        final String email = binding.email.getText().toString();
        final String pswd = binding.password.getText().toString();


        BackendlessUser user = new BackendlessUser();
        user.setEmail(email);
        user.setPassword(pswd);
        Backendless.UserService.login(email, pswd, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser backendlessUser) {
                Lo.g("# login handleResponse  " + backendlessUser);
                if (backendlessUser != null)
                    onAuthSuccess();
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                showProgressBar(false);
                Lo.ge("# login handleFault  " + backendlessFault);
                String msg = backendlessFault.getMessage();

                // if (msg.contains("Invalid login or password")) {
                Utils.showSnackBar(binding.content, msg, true);
                //binding.password.setError(getString(R.string.error_invalid_login));
                // binding.email.setError(getString(R.string.error_invalid_email));
                binding.email.requestFocus();
                binding.password.setText("");
                // }

            }
        }, true);


    }

    private void onAuthSuccess() {
        if (getActivity() != null) {
            authActivityListener.onAuthSuccess();
        }
    }


    private boolean isPasswordValid() {
        Lo.g("=> isPasswordValid: ");
        final String pswd = binding.password.getText().toString();

        //  Lo.ge("pswd: " + pswd);
        // Lo.ge("pswd: " + TextUtils.isEmpty(pswd));

        if (TextUtils.isEmpty(pswd)) {
            binding.password.setError(getString(R.string.error_field_required));
            binding.password.requestFocus();
            return false;
        } else if (pswd.length() < 6) {
            binding.password.setError(getString(R.string.error_invalid_password));
            binding.password.requestFocus();
            return false;
        }

        Utils.hideSoftKeyboard(getActivity(), binding.password);
        /* Clears focus */
        binding.email.setFocusableInTouchMode(false);
        binding.email.setFocusable(false);
        binding.email.setFocusableInTouchMode(true);
        binding.email.setFocusable(true);
        binding.password.setFocusableInTouchMode(false);
        binding.password.setFocusable(false);
        binding.password.setFocusableInTouchMode(true);
        binding.password.setFocusable(true);


        return true;
    }


}

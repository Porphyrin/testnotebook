package com.testnotebook.fragment;


import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.testnotebook.R;
import com.testnotebook.helper.Utils;


public class ProgressDialog extends DialogFragment {


    public static ProgressDialog newInstance(int title) {
        ProgressDialog d = new ProgressDialog();
        Bundle m_bundle = new Bundle();
        m_bundle.putInt("title", title);
        d.setArguments(m_bundle);
        return d;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View v = inflater.inflate(R.layout.dialog_progress_bar, container, false);


        if (getArguments() != null && getArguments().getInt("title") != 0) {
            ((TextView) v.findViewById(R.id.tvTitle)).setText(getString(getArguments().getInt("title")));
        }


        return v;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog m_dialog = new Dialog(getActivity(), R.style.ProgressDialog);
        m_dialog.setTitle(null);

        m_dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(m_dialog.getWindow().getAttributes());
        lp.width = (int) (Utils.getDisplayW() * 0.95);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        m_dialog.show();

        m_dialog.getWindow().setAttributes(lp);

        return m_dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        setCancelable(true);
        getDialog().setCanceledOnTouchOutside(true);


        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.70f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);

    }

    @Override
    public void onResume() {
        super.onResume();

        // setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }


}

package com.testnotebook.fragment;

import android.support.v4.app.Fragment;

public class BaseFragment extends Fragment {


    private ProgressDialog obscured_screen;

    public void showProgressBar(boolean show, int title) {
        startProgress(show, title);
    }

    public void showProgressBar(boolean show) {
        startProgress(show, 0);
    }

    private void startProgress(boolean show, int title) {
        if (getActivity() == null) return;
        if (obscured_screen == null)
            this.obscured_screen = ProgressDialog.newInstance(title);
        if (show) {
            this.obscured_screen.show(getActivity().getFragmentManager(), "ObscuredScreenProgressDialog");
        } else if (this.obscured_screen.isVisible()) {
            this.obscured_screen.dismiss();
            getActivity().getFragmentManager().beginTransaction().remove(this.obscured_screen).commit();
            this.obscured_screen = null;
        }
    }


}

package com.testnotebook.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.clientskeeper.R;
import com.clientskeeper.databinding.FragmentSignupBinding;
import com.clientskeeper.helper.AESHelper;
import com.clientskeeper.helper.ConstantsParse;
import com.clientskeeper.helper.Lo;
import com.clientskeeper.helper.Utils;
import com.clientskeeper.interfaces.OnAuthFragInteractionListener;


public class SignUpFragment extends Fragment implements View.OnClickListener {
    public final com.clientskeeper.helper.Lo Lo = new Lo(this);
    private ProgressDialog obscured_screen;
    private OnAuthFragInteractionListener authActivityListener;
    private FragmentSignupBinding binding;

    public SignUpFragment() {
        // Required empty public constructor
    }


    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_signup, container, false);
        binding.btnSigninEmail.setOnClickListener(this);
        binding.btnBack.setOnClickListener(this);



         /* Set autocomplete occupations  name */
//        if (getActivity() != null) {
//            // Lo.ge(new Gson().toJson(DBHelper.INSTANCE.getOccupationNames()));
//            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, DBHelper.INSTANCE.getOccupationNames());
//            binding.occupation.setAdapter(adapter);
//        }


        return binding.getRoot();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAuthFragInteractionListener) {
            authActivityListener = (OnAuthFragInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAuthFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        authActivityListener = null;
    }


    private void logInEmail() {

        // Check for a valid email/password.
        if (isEmailValid() && isPasswordValid() && isOccupationValid()) {
            signUp();
        }
    }

    private void signUp() {
        showProgressBar(true);

        final String email = binding.email.getText().toString();
        final String occupation = binding.occupation.getText().toString();
        final String pswd = binding.password.getText().toString();


        BackendlessUser user = new BackendlessUser();
        user.setEmail(email);
        user.setPassword(pswd);
        user.setProperty(ConstantsParse.FIELD_OCCUPATION_TITLE, Utils.getFirstUpperString(occupation));

        try {
            user.setProperty(ConstantsParse.SEED, AESHelper.getSalt());
        } catch (Exception e) {
            Utils.showDevToast(e.getMessage());
            Utils.showToast(getString(R.string.error_general));
            e.printStackTrace();
            return;
        }

        Backendless.UserService.register(user, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser backendlessUser) {
                        Lo.g("# register handleResponse " + backendlessUser);
                        if (backendlessUser != null)
                            onAuthSuccess();
                    }

                    @Override
                    public void handleFault(BackendlessFault backendlessFault) {
                        showProgressBar(false);
                        Lo.ge("# register handleFault  " + backendlessFault);
                        String msg = backendlessFault.getMessage();

                       // if (msg.contains("User already exists")) {
                            Utils.showSnackBar(binding.content, msg, true);
                            //binding.password.setError(getString(R.string.error_invalid_login));
                            // binding.email.setError(getString(R.string.error_invalid_email));
                            binding.email.requestFocus();
                            binding.password.setText("");
                       // }
                    }
                }
        );


//        ParseUser user = new ParseUser();
//        user.setUsername(email);
//        user.setPassword(pswd);
//        user.setEmail(email);
//        user.put(ConstantsParse.FIELD_OCCUPATION_TITLE, Utils.getFirstUpperString(occupation));
//        try {
//            user.put(ConstantsParse.SEED, AESHelper.getSalt());
//        } catch (Exception e) {
//            Utils.showToast(e.getMessage());
//            e.printStackTrace();
//            return;
//        }
//
//        user.signUpInBackground(new SignUpCallback() {
//            public void done(ParseException e) {
//                Lo.g("# callFunctionInBackground " + ConstantsParse.AUTH_TYPE_SIGNUP + ": " + e + " / object: ");
//                showProgressBar(false);
//                if (e == null) {
//                    onAuthSuccess();
//                } else {
//                    Utils.showDevToast(e.getMessage());
//                    Lo.ge("# callFunctionInBackground " + ConstantsParse.AUTH_TYPE_SIGNUP + ": " + e.getCode() + " / " + e);
//                    // {"code":202,"msg":"username aleks.plekhov@gmail.com already taken"}
//                    if (e.getCode() == 202 || e.getCode() == 203) {
//                        if (authActivityListener != null)
//                            authActivityListener.onShowLogInFrag(email);
//                    }
//                }
//            }
//        });


    }

    private void onAuthSuccess() {
        if (getActivity() != null)
            authActivityListener.onAuthSuccess();

    }

    public void showProgressBar(boolean show) {
        if (obscured_screen == null)
            obscured_screen = ProgressDialog.newInstance(0);
        if (show) {
            obscured_screen.show(getActivity().getFragmentManager(), "ObscuredScreenProgressDialog");
        } else if (obscured_screen.isVisible()) {
            obscured_screen.dismiss();
            obscured_screen = null;
            Log.d("", (obscured_screen == null) + "");
        }

    }


    private boolean isEmailValid() {
        final String email = binding.email.getText().toString();
        if (TextUtils.isEmpty(email)) {
            binding.email.setError(getString(R.string.error_field_required));
            binding.email.requestFocus();
            return false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            binding.email.setError(getString(R.string.error_invalid_email));
            binding.email.requestFocus();
            return false;
        }
        return true;
    }

    private boolean isPasswordValid() {
        final String pswd = binding.password.getText().toString();
        if (TextUtils.isEmpty(pswd)) {
            binding.password.setError(getString(R.string.error_field_required));
            binding.password.requestFocus();
            return false;
        } else if (pswd.length() < 6) {
            binding.password.setError(getString(R.string.error_invalid_password));
            binding.password.requestFocus();
            return false;
        }

        Utils.hideSoftKeyboard(getActivity(), binding.password);
        /* Clears focus */
        binding.email.setFocusableInTouchMode(false);
        binding.email.setFocusable(false);
        binding.password.setFocusableInTouchMode(false);
        binding.password.setFocusable(false);


        return true;
    }

    private boolean isOccupationValid() {
        final String occupation = binding.occupation.getText().toString();
        if (TextUtils.isEmpty(occupation)) {
            binding.occupation.setError(getString(R.string.error_field_required));
            binding.occupation.requestFocus();
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.btnSigninEmail:
                logInEmail();
                break;
            case R.id.btnBack:
                if (authActivityListener != null)
                    authActivityListener.onShowLogInFrag(null);
                break;

        }

    }


}

package com.testnotebook;

import android.app.Application;
import android.content.Context;

import com.backendless.Backendless;


/**
 * Created by admin on 12/9/15.
 */
public class GApplication extends Application {


    public static Context context;
    public static  boolean logPermission;


    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();
        logPermission = BuildConfig.DEBUG_MODE;
        Backendless.initApp(this, getString(R.string.bless_app_id), getString(R.string.bless_key), getString(R.string.bless_v));
    }

}

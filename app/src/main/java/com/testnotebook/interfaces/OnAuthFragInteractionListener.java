package com.testnotebook.interfaces;

public interface OnAuthFragInteractionListener {

    void onShowSignUpFrag();
    void onShowLogInFrag(String email);
    void onAuthSuccess();

}

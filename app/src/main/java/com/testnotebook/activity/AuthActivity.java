package com.testnotebook.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;

import com.backendless.Backendless;

import com.testnotebook.fragment.LoginFragment;
import com.testnotebook.fragment.SignUpFragment;
import com.testnotebook.helper.Lo;
import com.testnotebook.helper.Utils;
import com.testnotebook.interfaces.OnAuthFragInteractionListener;


public class AuthActivity extends AppCompatActivity implements OnAuthFragInteractionListener{
    public final Lo Lo = new Lo(this);

    public ActivityLoginBinding binding;
    private boolean firstStart = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        checkIsLogin();


        binding = DataBindingUtil.setContentView(AuthActivity.this, R.layout.activity_auth);


        showLoginFragment(null);


    }


    private void checkIsLogin() {
        String userToken =
                Backendless.UserService.loggedInUser();//UserTokenStorageFactory.instance().getStorage().get();
        Lo.g("userToken: " + userToken);
        if (userToken != null && !userToken.equals(""))
            startClientsActivity();

    }

    @Override
    protected void onStart() {
        super.onStart();
        firstStart = false;
    }

    private void showLoginFragment(String email) {
        FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
        if (!firstStart)
            trans.setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_right);
        trans.replace(R.id.content_frame, LoginFragment.newInstance(email), "LoginFragment").commit();
    }

    private void showSignUpFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
                .replace(R.id.content_frame, SignUpFragment.newInstance(), "SignUpFragment").commit();
    }


    @Override
    public void onShowSignUpFrag() {
        showSignUpFragment();
    }

    @Override
    public void onShowLogInFrag(String email) {
        if (email != null) {
            showLoginFragment(email);
            Utils.showSnackBar(binding.contentFrame, getString(R.string.error_already_signup), true);
        } else
            showLoginFragment(null);

    }

    @Override
    public void onAuthSuccess() {
        startClientsActivity();
    }


    @Override
    public void onEmailChosenForReset(final String text) {
//        ParseUser.requestPasswordResetInBackground(text, new RequestPasswordResetCallback() {
//            @Override
//            public void done(ParseException e) {
//                Lo.g("=> onEmailChosenForReset: " + text + " / " + e);
//                if (e == null) {
//                    Utils.showSnackBar(binding.contentFrame, getString(R.string.success_reset_pswd), true);
//                } else {
//                    Utils.showSnackBar(binding.contentFrame, getString(R.string.error_reset_pswd), true);
//                    Lo.ge("# onEmailChosenForReset e:  " + e.getCode() + " /:  " + e.getMessage());
//                }
//            }
//        });

    }

    private void startClientsActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
        Utils.showLeftAnimation(this);

    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isOpenedSignUp()) {
                showLoginFragment(null);
                return true;
            }
        } else
            return super.onKeyUp(keyCode, event);
        return super.onKeyUp(keyCode, event);
    }


    private boolean isOpenedSignUp() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager != null) {
            Fragment fragment_container = manager.findFragmentById(R.id.content_frame);
            return fragment_container instanceof SignUpFragment;
        }
        return false;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
        Lo.g("=> onActivityResult: " + resultCode);

        android.support.v4.app.Fragment fragment2 = getSupportFragmentManager().findFragmentByTag("LoginFragment");
        if (fragment2 != null) {
            Lo.g("=> onActivityResult LoginFragment: ");
            fragment2.onActivityResult(requestCode, resultCode, data);
        }
    }


}





package com.testnotebook.helper;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.testnotebook.GApplication;


public final class Utils {
    public static final Lo Lo = new Lo(GApplication.context);
    private static int display_w = 0;
    private static int display_h = 0;

    private Utils() {
    }


    public static boolean isConnectingToInternet(boolean show_alert, Integer idString) {
        ConnectivityManager connectivity = (ConnectivityManager) GApplication.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo ni = connectivity.getActiveNetworkInfo();
            if (ni == null) {
                if (show_alert)
                    Toast.makeText(GApplication.context, idString == null ? R.string.error_label_no_internet : idString, Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }


    public static void showToast(String text) {
        Toast.makeText(GApplication.context, text, Toast.LENGTH_LONG).show();
    }


    public static int getDisplayW() {
        if (display_w != 0)
            return display_w;
        else {
            WindowManager wm = (WindowManager) GApplication.context.getSystemService(Context.WINDOW_SERVICE);

            Display display = wm.getDefaultDisplay();
            if (Build.VERSION.SDK_INT >= 13) {
                Point size = new Point();
                display.getSize(size);
                display_w = size.x;
            } else {
                display_w = wm.getDefaultDisplay().getWidth();
            }

            return display_w;
        }

    }

    public static int getDisplayH() {
        if (display_h != 0)
            return display_h;
        else {
            WindowManager wm = (WindowManager) GApplication.context.getSystemService(Context.WINDOW_SERVICE);

            Display display = wm.getDefaultDisplay();

            if (Build.VERSION.SDK_INT >= 13) {
                Point size = new Point();
                display.getSize(size);
                display_h = size.y;
            } else {
                display_h = wm.getDefaultDisplay().getHeight();
            }


            return display_h;
        }

    }

    public static void showSnackBar(View content, String title, boolean isError) {
        Snackbar snackbar = Snackbar.make(content, title, Snackbar.LENGTH_LONG);
        if (isError) {
            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
        }
        snackbar.show();

    }

    public static void showLeftAnimation(final Activity activity) {
        // if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.DONUT)
        activity.overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    public static void showRightAnimation(final Activity activity) {
        // if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.DONUT)
        activity.overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_right);
    }

    public static boolean isEmailValid(EditText view) {
        final String email = view.getText().toString();
        Lo.g("=> isEmailValid: " + email);
        if (TextUtils.isEmpty(email)) {
            view.setError(GApplication.context.getString(R.string.error_field_required));
            view.requestFocus();
            return false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            view.setError(GApplication.context.getString(R.string.error_invalid_email));
            view.requestFocus();
            return false;
        }
        return true;
    }

    public static void hideSoftKeyboard(Context ctx, View view) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

}

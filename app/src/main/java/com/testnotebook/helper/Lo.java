package com.testnotebook.helper;

import android.util.Log;


import com.testnotebook.GApplication;
import com.testnotebook.R;

import java.io.Serializable;

public class Lo implements Serializable {

    private static final String DEFAULT_TAG_PREFIX = GApplication.context.getResources().getString(R.string.app_name);
    private static final String DEFAULT_TAG = GApplication.context.getResources().getString(R.string.app_name);
    private String tag;


    public Lo(Object caller) {
        this.tag = caller.getClass().getSimpleName();
    }

    public void g(String logString) {
        if (GApplication.logPermission) {
            Log.d(getFullTag(), String.format("%s", logString));
        }
    }
    public void ge(String logString) {
        if (GApplication.logPermission) {
            Log.e(getFullTag(), String.format("%s", logString));
        }
    }
    public void g(String logFormat, Object... args) {
        if (GApplication.logPermission) {
            Log.d(getFullTag(), String.format(logFormat, args));
        }
    }

    public void g(Object arg) {
        if (GApplication.logPermission) {
            Log.d(getFullTag(), String.format("%s", arg));
        }
    }

    public String getFullTag() {
        return String.format("%s %s", DEFAULT_TAG_PREFIX, tag);
    }

    /**
     * Static log without tag
     */

    public static void gg(String logString) {
        if (logString == null) {
            Log.d(DEFAULT_TAG, "null");
            return;
        }
        Log.d(DEFAULT_TAG, logString);
    }

    public static void gg(String logFormat, Object... args) {
        Log.d(DEFAULT_TAG, String.format(logFormat, args));
    }

    public static void gg(Object arg) {
        if (arg == null) {
            Log.d(DEFAULT_TAG, "null");
            return;
        }
        Log.d(DEFAULT_TAG, arg.toString());
    }

    /**
     * Static log with tag
     */

    public static String getFullTag(String tag) {
        return String.format("%s %s", DEFAULT_TAG_PREFIX, tag);
    }

    public static void gt(String logTag, String logString) {
        Log.d(getFullTag(logTag), logString);
    }

    public static void gt(String logTag, String logFormat, Object... args) {
        Log.d(getFullTag(logTag), String.format(logFormat, args));
    }

    public static void gt(String logTag, Object arg) {
        Log.d(getFullTag(logTag), String.format("%s", arg.toString()));
    }
}